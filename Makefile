SHELL := /bin/bash
Compiler := node_modules/typescript/bin/tsc

.PHONY: int

all: configure
	$(Compiler)
	node build.js

configure:
	npm install
	@if [ ! -d "data" ]; then git clone git@gitlab.com:ExtropicStudios/data.git; fi
	@if [ ! -d "static" ]; then git clone git@gitlab.com:ExtropicStudios/static.git; fi

int:
	jekyll build int-main --destination int

install: configure
	$(Compiler)
	node deploy.js

clean:
	rm *.js
