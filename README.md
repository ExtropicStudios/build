# How to use

Put page modules in the `data` directory. A page module has 3 files:

- data.json
- template.hbs
- transform.ts

`data` is applied to the `template` after running it through the function
exported by `transform`. The resulting index.html file is placed in the
corresponding path in the `static` directory.

# cool tools

- http://www.typescriptlang.org/
- http://phaser.io/
- http://crystal-lang.org/

# TODOs

- [X] Move date calculation and other variables to global level
- [X] Add global header and footer
- [ ] Compile CSS from SASS?
- [ ] Add command to commit changes to data & static simultaneously
- [ ] Add command to deploy to extropicstudios.com
- [ ] Generated page: tweets
- [ ] Generated page: instagram
- [ ] Generated page: blog
- [ ] Generated page: bandcamp
- [ ] Generated page: travel
- [ ] Generated page: ghost conlang
- [ ] Generated page: code68k
- [ ] Generate RSS feeds for changes
- [ ] Cloudwatch style dashboard for life stats
- [ ] Static page to document workspace setup
- [ ] Generate dot graphs from book tags
- [ ] Generate list of links from IRC chat logs
