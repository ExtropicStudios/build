// Maybe replace with TSD: https://stackoverflow.com/questions/29964620/visual-studio-code-download-node-d-ts/30023847#30023847
declare function require(name:string);

let fs = require('fs');
let handlebars = require('handlebars');

// Partials are templates that can be included in other templates.
let partials = [
  'footer',
];

partials.forEach((p) => {
  handlebars.registerPartial(p, fs.readFileSync('./partials/' + p + '.hbs', 'utf8'));
});

// TODO: scan directories instead of having to enumerate them here
// TODO: have this value passed in from the makefile
let pages = [
  'mathema/books',
  'mathema/games',
  'mathema/movies',
  'mathema/podcasts',
  'mathema/tv',
  'int/cool-links',
  'ink/psyche/lines',
];

let now = new Date();
let formatted_date = now.getDate() + " " + now.toLocaleDateString("en-us", {month: 'short'}) + " " + now.getFullYear();

pages.forEach((name) => {
  let transform = require('./data/' + name + '/transform.js');
  let data = transform(require('./data/' + name + '/data.json'));

  // If transform.js returns an array of objects, generate one page per object,
  // using the `title` property as the name of the page.
  if (data instanceof Array) {
    data.forEach((datum) => {
      datum.date = formatted_date;
      let template = handlebars.compile(fs.readFileSync('./data/' + name + '/template.hbs', 'utf8'));
      fs.writeFileSync('./static/' + name + '/' + datum.title + '.html', template(datum));
    });
  // Otherwise just generate a single page and call it index.html.
  } else {
    data.date = formatted_date;
    let template = handlebars.compile(fs.readFileSync('./data/' + name + '/template.hbs', 'utf8'));
    fs.writeFileSync('./static/' + name + '/index.html', template(data));
  }
});
